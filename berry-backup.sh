#!/bin/sh
#based on https://blog.interlinked.org/tutorials/rsync_time_machine.html 
#start with backup_probook and change to meet your needs.
#
#I have the following line in my crontab to support running this daily:
#
#    41 15 * * * /projects/berry-backup/berry-backup.sh backup_probook 2>&1 >> /var/log/bcronlog
#

#date=`date "+%Y-%m-%dT%H:%M:%S"`
#echo $date": starting home backup"
#rsync -azP --link-dest=PATHTOBACKUP/current $SOURCE $HOST:PATHTOBACKUP/back-$date
#ssh $HOST "rm -f PATHTOBACKUP/current && ln -s back-$date PATHTOBACKUP/current"

# arguments
# $1 source folder path (note that trailing / should be included!)
# $2 destination host
# $3 dest folder path
# note that you should have set up public key authentication for this to work properly.
backupdirectory() {
	date=`date "+%Y-%m-%dT%H:%M:%S"`
	SOURCE="$1"
	DHOST="$2"
	DESTDIR="$3"
	SOURCENAME="`basename "$SOURCE"`"
	BACKNAME="$SOURCENAME"-$date
	CURRENTNAME="$SOURCENAME""-current"
	echo $date": starting incremental backup of '$SOURCE' to '$DESTDIR' on '$DHOST'"
	rsync -azP --link-dest="$DESTDIR""$CURRENTNAME" "$SOURCE" $DHOST:"$DESTDIR"/"$BACKNAME"
	ssh $DHOST "rm -f \"$DESTDIR\"/\"$CURRENTNAME\" && ln -s \"$BACKNAME\" \"$DESTDIR\"/\"$CURRENTNAME\""
}

backup_probook() {
	backupdirectory "/home/" homebuilt "/bdisk1/probook/"
	backupdirectory "/projects/" homebuilt "/bdisk1/probook/"
}

# copy one file at a time from testhelper to test, run this target, and see the incremental backups working
backup_test() {
	backupdirectory "/projects/berry-backup/test/" homebuilt "/bdisk1/probook/"
}


eval "$1"

#0 12 * * * ( echo `date`": starting home backup" && rsync -az /home vancan1ty@homebuilt:/bdisk1/probook/ ) 2>&1 >> /var/log/bcronlog
#0 14 * * * ( echo `date`": staring projects backup" && rsync -az /projects vancan1ty@homebuilt:/bdisk1/probook/ ) 2&>1 >> /var/log/bcronlog

